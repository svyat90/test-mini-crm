<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class EventsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request, $user_id)
    {
        $user = User::find($user_id);

        return view('dashboard', compact('user'));
    }
}
