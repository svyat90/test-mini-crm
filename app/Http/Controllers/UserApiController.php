<?php

namespace App\Http\Controllers;

use App\Event;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserApiController extends Controller
{
    private function check_admin_password($admin_id, $password_admin)
    {
        $admin = User::find($admin_id)->first();

        if (Auth::attempt(['email' => $admin->email, 'password' => $password_admin])) {
            return true;
        }

        return false;
    }

    public function update_login(Request $request)
    {
        $this->validate($request, [
            'user_id' => 'required|integer',
            'admin_id' => 'required|integer',
            'login' => 'required',
            'password_admin' => 'required'
        ]);

        $user_id = $request->input('user_id');
        $admin_id = $request->input('admin_id');
        $login = $request->input('login');
        $password_admin = $request->input('password_admin');

        $response = false;
        $error = false;

        if($this->check_admin_password($admin_id, $password_admin)) {
            $response = User::find($user_id)->update([
                'email' => $login
            ]);
        } else {
            $error = 'Admin password not match';
        }

        return response()->json([
            'response' => ($response) ? true : false,
            'error' => $error
        ]);
    }

    public function update_password(Request $request)
    {
        $this->validate($request, [
            'user_id' => 'required|integer',
            'admin_id' => 'required|integer',
            'password' => 'required',
            'password_admin' => 'required'
        ]);

        $user_id = $request->input('user_id');
        $admin_id = $request->input('admin_id');
        $password = $request->input('password');
        $password_admin = $request->input('password_admin');

        $response = false;
        $error = false;

        if($this->check_admin_password($admin_id, $password_admin)) {
            $response = User::find($user_id)->update([
                'password' => bcrypt($password)
            ]);
        } else {
            $error = 'Admin password not match';
        }

        return response()->json([
            'response' => ($response) ? true : false,
            'error' => $error
        ]);
    }

    public function delete_user(Request $request)
    {
        $this->validate($request, [
           'user_id' => 'required|integer'
        ]);

        $user_id = $request->input('user_id');

        $deleted_rows = Event::where('user_id', $user_id)->delete();

        $message = "Events not match";
        if ($deleted_rows) {
            $message = "Deleted ".$deleted_rows." events.";
        }

        User::find($user_id)->delete();

        return response()->json([
            'response' => ($message) ? $message : false,
        ]);
    }
}
