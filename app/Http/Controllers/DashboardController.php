<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (Auth::user()->admin()) {

            $users = User::where('is_admin', 0)->get();

            $start = new Carbon('first day of this month');
            $end = new Carbon('last day of this month');

            foreach ($users as $user) {
                $events = $user->events()
                    ->where('date', '>=', $start->subDay()->timestamp)
                    ->where('date', '<=', $end->addDay()->timestamp)->get()->toArray();

                foreach ($events as $item) {
                    $user->count_hours += $item['quantity'];
                }
            }

            return view('users', compact('users'));
        }

        return view('dashboard');
    }
}
