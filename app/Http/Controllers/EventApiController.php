<?php

namespace App\Http\Controllers;

use App\Event;
use App\User;
use Illuminate\Http\Request;

class EventApiController extends Controller
{
    public function list(Request $request)
    {
        $this->validate($request, [
            'user_id' => 'required|integer',
            'start' => 'required|integer',
            'end' => 'required|integer'
        ]);

        $user_id = $request->input('user_id');
        $start = $request->input('start');
        $end = $request->input('end');

        $events = Event::where('date', '>', $start)
            ->where('date', '<', $end)
            ->where('user_id', $user_id)->get();

        return response()->json([
            'response' => ($events->count() > 0) ? true : false,
            'data' => $events
        ]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'user_id' => 'required|integer',
            'quantity' => 'required|integer',
            'date' => 'required|integer',
            'comment' => 'required'
        ]);

        $user_id = $request->input('user_id');
        $user = User::find($user_id);

        $event = $user->events()->create([
            'quantity' => $request->input('quantity'),
            'date' => $request->input('date'),
            'comment' => $request->input('comment')
        ]);

        return response()->json([
            'response' => ($event) ? true : false
        ]);
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'event_id' => 'required|integer',
            'quantity' => 'required|integer',
            'comment' => 'required'
        ]);

        $event_id = $request->input('event_id');
        $quantity = $request->input('quantity');
        $comment = $request->input('comment');

        $event = Event::find($event_id)->update([
            'quantity' => $quantity,
            'comment' => $comment
        ]);

        return response()->json([
            'response' => ($event) ? true : false
        ]);
    }
}
