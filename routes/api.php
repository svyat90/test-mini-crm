<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('events', 'EventApiController@list');
Route::post('event/create', 'EventApiController@store');
Route::post('event/update', 'EventApiController@update');
Route::post('user/update_login', 'UserApiController@update_login');
Route::post('user/update_password', 'UserApiController@update_password');
Route::post('user/delete_user', 'UserApiController@delete_user');