<script>
    $(window).load(function() {

        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();
        var started;
        var categoryClass;

        var calendar = $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            selectable: true,
            selectHelper: true,
            select: function(start, end, allDay) {
                $('#fc_create').click();

                started = start;
                ended = end

                $(".antosubmit").on("click", function() {
                    var quantity = $("#quantity-new").val();
                    var comment = $("#comment-new").val();
                    var user_id = $("#user_id").val();

                    if (end) {
                        ended = end
                    }

                    categoryClass = $("#event_type").val();

                    createEvent(quantity, comment, started.unix(), user_id);

                    $('#quantity-new').val('');
                    $('#comment-new').val('');

                    calendar.fullCalendar('unselect');

                    $('.antoclose').click();
                    $(".antosubmit").off("click");

                    return false;
                });
            },
            eventClick: function(calEvent, jsEvent, view) {
                // alert(calEvent.user_id, jsEvent, view);

                $('#fc_edit').click();
                $('#quantity-edit').val(calEvent.quantity);
                $('#comment-edit').val(calEvent.comment);
                categoryClass = $("#event_type").val();

                $(".antosubmit2").on("click", function() {
                    var quantity = $("#quantity-edit").val();

                    calEvent.comment = $("#comment-edit").val();
                    calEvent.title = $("#quantity-edit").val()+' '+calEvent.comment;

                    calendar.fullCalendar('updateEvent', calEvent);

                    updateEvent(quantity, calEvent.comment, calEvent.id, calEvent.user_id);

                    $('.antoclose2').click();
                    $(".antosubmit2").off("click");
                });
                calendar.fullCalendar('unselect');
            },
            editable: true,
            events: function(start, end, timezone, callback) {
                var user_id = $("#user_id").val();

                var form = new FormData();

                form.append('start', start.unix());
                form.append('end', end.unix());
                form.append('user_id', user_id);

                $.ajax({
                    url: 'api/events',
                    type: 'POST',
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form,
                    success: function(result) {
                        var events = [];

                        if (result.response) {
                            console.log(result.data);
                            result.data.forEach(function(item) {
                                events.push({
                                    id: item.id,
                                    user_id: item.user_id,
                                    title: item.quantity+' hours '+item.comment,
                                    quantity: item.quantity,
                                    comment: item.comment,
                                    start: moment(new Date(item.date*1000)).format('YYYY/MM/DD')// will be parsed
                                });
                            });
                        }

                        callback(events);
                    }
                });
            }
        });

        function createEvent(quantity, comment, date, user_id) {
            var form = new FormData();

            form.append('quantity', quantity);
            form.append('comment', comment);
            form.append('date', date);
            form.append('user_id', user_id);

            $.ajax({
                url: 'api/event/create',
                data: form,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                success:function(result) {
                    if (result.response) {
                        calendar.fullCalendar('renderEvent', {
                                title: quantity + ' hour ' + comment,
                                start: started
                            },
                            true
                        );

                        new PNotify({
                            title: 'Success',
                            text: 'Successfully created event!',
                            type: 'success'
                        });
                    }
                },
                error: function (result) {
                    var errors = result.responseJSON.errors;
                    var message = result.responseJSON.message;

                    for(var error in errors) {
                        console.log(errors[error][0])
                        new PNotify({
                            title: message,
                            text: errors[error][0],
                            type: 'error'
                        });
                    }
                }
            });
        }

        function updateEvent(quantity, comment, event_id, user_id) {
            var form = new FormData();

            form.append('quantity', quantity);
            form.append('comment', comment);
            form.append('event_id', event_id);
            form.append('user_id', user_id);

            $.ajax({
                url: 'api/event/update',
                data: form,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                success:function(result) {
                    if (result.response) {
                        new PNotify({
                            title: 'Success',
                            text: 'Successfully updated event!',
                            type: 'success'
                        });
                    }
                },
                error: function (result) {
                    var errors = result.responseJSON.errors;
                    var message = result.responseJSON.message;

                    for(var error in errors) {
                        console.log(errors[error][0])
                        new PNotify({
                            title: message,
                            text: errors[error][0],
                            type: 'error'
                        });
                    }
                }
            });
        }
    });
</script>