@extends('layouts.crm')

@section('styles')
    <link href="{{ asset('assets/css/calendar/fullcalendar.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/calendar/fullcalendar.print.css') }}" rel="stylesheet" media="print">
@endsection

@section('left-sidebar')
    @include('left-sidebar')
@endsection

@section('top-sidebar')
    @include('top-sidebar')
@endsection

@section('content')
    @include('full-calendar')
@endsection

@section('footer')

    <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>

    <script src="{{ asset('assets/js/nprogress.js') }}"></script>

    <!-- bootstrap progress js -->
    <script src="{{ asset('assets/js/progressbar/bootstrap-progressbar.min.js') }}"></script>
    <script src="{{ asset('assets/js/nicescroll/jquery.nicescroll.min.js') }}"></script>
    <!-- icheck -->
    <script src="{{ asset('assets/js/icheck/icheck.min.js') }}"></script>

    <script src="{{ asset('assets/js/custom.js') }}"></script>

    <script src="{{ asset('assets/js/moment/moment.min.js') }}"></script>
    <script src="{{ asset('assets/js/calendar/fullcalendar.min.js') }}"></script>
    <!-- pace -->
    <script src="{{ asset('assets/js/pace/pace.min.js') }}"></script>

    <!-- PNotify -->
    <script type="text/javascript" src="{{ asset('assets/js/notify/pnotify.core.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/notify/pnotify.buttons.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/notify/pnotify.nonblock.js') }}"></script>

    @if (Auth::user()->admin())
        @include('footer-admin')
    @else
        @include('footer-user')
    @endif

@endsection