<script>
    $(document).ready(function() {

        function changeLogin() {
            var user_id = $("#user-id").val();
            var admin_id = $("#admin-id").val();
            var login = $("#change-login").val();
            var password_admin = $("#password-change-login").val();

            var form = new FormData();

            form.append('login', login);
            form.append('password_admin', password_admin);
            form.append('user_id', user_id);
            form.append('admin_id', admin_id);

            $.ajax({
                url: 'http://'+window.location.hostname+':'+window.location.port+'/api/user/update_login',
                data: form,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                success:function(result) {
                    if (result.response) {
                        new PNotify({
                            title: 'Success',
                            text: 'Successfully created event!',
                            type: 'success'
                        });
                    } else {
                        new PNotify({
                            title: 'Error',
                            text: result.error,
                            type: 'error'
                        });
                    }
                },
                error: function (result) {
                    var errors = result.responseJSON.errors;
                    var message = result.responseJSON.message;

                    for(var error in errors) {
                        console.log(errors[error][0])
                        new PNotify({
                            title: message,
                            text: errors[error][0],
                            type: 'error'
                        });
                    }
                }
            });
        }

        function changePassword() {
            var user_id = $("#user-id").val();
            var admin_id = $("#admin-id").val();
            var password = $("#change-password").val();
            var password_admin = $("#change-password-admin").val();

            var form = new FormData();

            form.append('password', password);
            form.append('password_admin', password_admin);
            form.append('user_id', user_id);
            form.append('admin_id', admin_id);

            $.ajax({
                url: 'http://'+window.location.hostname+':'+window.location.port+'/api/user/update_password',
                data: form,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                success:function(result) {
                    if (result.response) {
                        new PNotify({
                            title: 'Success',
                            text: 'Successfully created event!',
                            type: 'success'
                        });
                    } else {
                        new PNotify({
                            title: 'Error',
                            text: result.error,
                            type: 'error'
                        });
                    }
                },
                error: function (result) {
                    var errors = result.responseJSON.errors;
                    var message = result.responseJSON.message;

                    for(var error in errors) {
                        console.log(errors[error][0])
                        new PNotify({
                            title: message,
                            text: errors[error][0],
                            type: 'error'
                        });
                    }
                }
            });
        }
        
        function deleteUser() {
            var user_id = $("#user-id").val();

            var form = new FormData();

            form.append('user_id', user_id);

            $.ajax({
                url: 'http://'+window.location.hostname+':'+window.location.port+'/api/user/delete_user',
                data: form,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                success:function(result) {
                    if (result.response) {
                        new PNotify({
                            title: 'Successfully deleted user',
                            text: result.response,
                            type: 'success'
                        });
                        setTimeout(window.location.href = 'http://'+window.location.hostname+':'+window.location.port+'/dashboard', 2000);
                    } else {
                        new PNotify({
                            title: 'Error',
                            text: result.error,
                            type: 'error'
                        });
                    }
                }
            });
        }

        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            selectable: true,
            selectHelper: true,
            editable: false,
            events: function(start, end, timezone, callback) {
                var user_id = "{{ Request::segment(2) }}";

                var form = new FormData();

                form.append('start', start.unix());
                form.append('end', end.unix());
                form.append('user_id', user_id);

                $.ajax({
                    url: 'http://'+window.location.hostname+':'+window.location.port+'/api/events',
                    type: 'POST',
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form,
                    success: function(result) {
                        var events = [];
                        console.log(result);

                        if (result.response) {
                            console.log(result.data);
                            result.data.forEach(function(item) {
                                events.push({
                                    id: item.id,
                                    user_id: item.user_id,
                                    title: item.quantity+' hours '+item.comment,
                                    quantity: item.quantity,
                                    comment: item.comment,
                                    start: moment(new Date(item.date*1000)).format('YYYY/MM/DD')// will be parsed
                                });
                            });
                        }

                        callback(events);
                    }
                });
            }
        });
        
        $("form[name=change-login]").submit(function (event) {
            event.preventDefault();
            changeLogin();
        });

        $("form[name=change-password]").submit(function (event) {
            event.preventDefault();
            changePassword();
        });
        
        $("#delete-user").click(function (event) {
            console.log('delete');
            deleteUser();
        })
    });
</script>